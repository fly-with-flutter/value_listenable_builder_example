# Fly With Flutter
## Value Listenable Builder Example
This is an example of how to use ValueNotifier and ValueListenableBuilder in Flutter to update screen due to value changes 
without use the **setState** method or needing to rebuilt all the screen
<div style="display:flex;justify-content:center">
<img src="https://i.ibb.co/V2t7mhY/ezgif-com-video-to-gif.gif" alt="drawing" width="200"  />
</div>

## instructions
1- Create a ValueNotifier: Declare a ValueNotifier variable and initialize it with an initial value. For example, to create a ValueNotifier for an integer value with an initial value of 0:
```
ValueNotifier<int> _counter = ValueNotifier<int>(0);
```
2- Listen to ValueNotifier changes: Wrap the part of your code that needs to be notified about changes in a ValueListenableBuilder widget. This widget takes the ValueNotifier as a parameter and provides a builder function that gets called whenever the value changes. For example:
```
ValueListenableBuilder(
  valueListenable: _counter,
  builder: (BuildContext context, int value, Widget? child) {
    // Use the updated value here
    return Text(value.toString());
  },
)
```
3- Update the ValueNotifier value: To update the value of the ValueNotifier, simply assign a new value to it. For example, to increment the counter:
```
_counter.value++;
```
4-That's it! With the ValueNotifier setup, any changes to the value will automatically trigger the builder function within the ValueListenableBuilder widget, allowing you to update your UI accordingly.

----------

> Remember to dispose of the ValueNotifier when it's no longer needed, typically in the dispose() method of your Stateful widget, to prevent memory leaks

----------
**Don't Mess Up And Follow Me For More**

[LinkedIn](https://www.linkedin.com/in/omar-kaialy-988531210)

[Telegram](https://t.me/omarlord1221)

[Gitlab](https://www.gitlab.com/omarlord1221)
